clean-server:
	rm -rf server/dst
	mkdir server/dst

compile-server: clean-server
	find server/src -name '*.ts' | xargs ./node_modules/.bin/tsc --sourceMap --noImplicitAny -m commonjs --outDir server/dst

test-server: 
	NODE_ENV=test ./node_modules/.bin/istanbul cover --root ./server ./server/all-tests.js