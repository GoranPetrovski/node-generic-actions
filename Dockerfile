FROM node:6.9.1-slim
MAINTAINER Goran Petrovski <goran.petrovski@alite-international.com>

ADD . /app
RUN cd /app; npm install
EXPOSE 8300
CMD ["node", "/app/server/dst/index.js"]