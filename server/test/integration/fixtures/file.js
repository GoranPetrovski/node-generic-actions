var through = require('through2');
var fs = require('fs');

/**
 * Gets the stream to a fake file.
 * @param {Object} [opt] additional options.
 * @param {*} [opt.content="Hello&nbsp;world\n"] optional file contents. If not provided, the file content default is
 *                          <code>'Hello world\n'</code>.
 * @returns {Stream} a stream containing the fake file data.
 */
exports.fakeFile = function fakeFile(opt) {
    opt = opt || {};
    opt.content = opt.content || 'Hello world\n';
    var s = through();
    s.length = new Buffer(opt.content).length;
    setTimeout(function() {
        s.push(opt.content);
        s.push(null);
    }, 1);
    return s;
};

var path = require('path');

/**
 * Gets a static test file.
 * @param {String} fileName the name of the file located in <code>[project root]/test/integration/fixtures/testfiles</code>.
 * @returns {"fs".ReadStream} a readable stream for the file.
 */
exports.testFile = function testFile(fileName) {
    return fs.createReadStream(path.resolve(__dirname, 'testfiles', fileName));
};