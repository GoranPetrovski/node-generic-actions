exports.baseuid = '5d8b39f5-d6f0-4b7c-9292-23bcec07923';

exports.getInfo = function getInfo(opt) {
    if (typeof(opt.uid) !== "number")
        throw new TypeError("Must provide user number");
    return {
        id: exports.baseuid + opt.uid,
        email: generateEmail(opt.uid),
        password: opt.upass ? opt.upass : 'testtest' + opt.uid,
        secretToken: exports.baseuid + opt.uid,
        enabled: true,
        dateCreated: new Date()//'Tue Oct 01 2013 17:08:06 GMT+0200 (CEST)')
    };
};

exports.getEmailPass = function(opt) {
    var info = exports.getInfo(opt);
    return { email: info.email, password: info.password };
};

/**
 * Generates a test e-mail address.
 * @param {Number} uid the user number.
 * @returns {string} the e-mail address.
 * @type {Function}
 */
exports.generateEmail = generateEmail;

/**
 * Generates a test e-mail address.
 * @param {Number} uid the user number.
 * @returns {string} the e-mail address.
 */
function generateEmail(uid) { return 'gorgi.kosev+test' + uid + '@gmail.com'; }