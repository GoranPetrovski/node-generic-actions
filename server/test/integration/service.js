
var tt = require('./lib/dbtest');
var user = require('./lib/user');
var fs = require('fs');

var Promise = require('bluebird');
var _ = require('hidash');

var uuid = require('uuid');
var stream = require('stream');
var str = require('string-to-stream');


/**
 * build mail body
 */
// {	
// 	"packageId":"build-mail-id",
// 	"html": "<!DOCTYPE html><html><body><h1>Hi Goran!!!</h1><p>How are you today?</p></body></html>",
// 	"attachmentLink": "https://rendered-documents-test-alonso-ger.s3-eu-west-1.amazonaws.com/d6dc643e-204d-425e-bf6d-1338c2100e7c",
// 	"to": "goki.petrovski@gmail.com",
// 	"subject": "Project-G"
// }

exports.run = function () {
    return new Promise(function (resolve) {

        tt.test('integration tests', function (t) {

            const u = user({ uid: 1 });

            const html = "<html><body><h1>Hiii <%= user.name %> !!!</h1></body></html>";
            const data = {
                "user": {
                    "name": "Goran"
                }
            }
            const link = "https://rendered-documents-test-alonso-ger.s3-eu-west-1.amazonaws.com/03c8f4f2-88cb-496a-a9d5-5b77cb3e3245";
            const bucket = "rendered-documents-test-alonso-ger";
            const path = "save-key-6-2-17";
            const blobId = "id123";
            const save = true;
            const returnType = "pdf";

            // t.test('string to pdf', function (t) {
            //     var wstream = fs.createWriteStream('myBinaryFile.pdf');
            //     var rstream = u.post("/document/string-pdf", { html, data });
            //     rstream.pipe(wstream);

            //     return new Promise(function (resolve) {
            //         rstream.on('end', function (chunk) {
            //             wstream.end();
            //             const fileSizeInBytes = fs.statSync("myBinaryFile.pdf")["size"];
            //             console.log("Size of file", fileSizeInBytes);
            //             t.ok(fileSizeInBytes > 0, "should be able to convert string to pdf");
            //             resolve();
            //         });
            //     });
            // });

            // t.test('render sting to pdf', function (t) {
            //     return u.post("/document/render/string", { html: html, data: data }).then(function (response) {
            //         t.ok(response.body.renderedHtml, "Should be able to render html string");
            //         var wstream = fs.createWriteStream('myBinaryFile.pdf');

            //         var rstream = u.post("/document/render/string-pdf", { html, data });
            //         rstream.pipe(wstream);
            //         return new Promise(function (resolve) {
            //             rstream.on('end', function (chunk) {
            //                 console.log("end event");
            //                 wstream.end();
            //                 const fileSizeInBytes = fs.statSync("myBinaryFile.pdf")["size"];
            //                 console.log("Size of file", fileSizeInBytes);
            //                 t.ok(fileSizeInBytes > 0, "should be able to render string and convert to pdf");
            //                 resolve();
            //             });
            //         });

            // return u.post('/document/render/string/pdf', { html: html, data: data }).then(function (response) {
            //     var wstream = fs.createWriteStream('myBinaryFile.pdf');
            //     response.on('data', function (chunk) {
            //         console.log("data event: ", chunk);
            //         wstream.write(chunk);
            //     })
            //     response.on('end', function (chunk) {
            //         console.log("end event");
            //         wstream.end();
            //         const fileSizeInBytes = fs.statSync("myBinaryFile.pdf")["size"]
            //         t.ok(fileSizeInBytes > 0, "should be able to render string and convert to pdf");
            //     });
            // })
            //     });;
            // });

            t.test('save template', function (t) {
                return u.post("/document/save-template", { bucket, path, html }).then(function (response) {
                    console.log("download Link for template");
                    console.log(response.body.downloadLink);
                    t.ok(response.body.downloadLink, "Should be able to save template");
                });
            });

            //     t.test('render template', function (t) {
            //        var wstream = fs.createWriteStream('myBinaryFile.pdf');
            //         return tt.errorTest(
            //             t,
            //             u.post('/document/render-template', { link, data, "return-type": returnType }),
            //             (400, "Should be set correct parametars")
            //         );


            //         if (typeof (response) !== "string") {
            //             var rstream = response;
            //             rstream.pipe(wstream);
            //             return new Promise(function (resolve) {
            //                 rstream.on('end', function (chunk) {
            //                     wstream.end();
            //                     const fileSizeInBytes = fs.statSync("myBinaryFile.pdf")["size"];
            //                     console.log("AASSSSSA");
            //                     console.log("Size of file", fileSizeInBytes);
            //                     t.ok(fileSizeInBytes > 0, "should be able to convert string to pdf");
            //                     resolve();
            //                 });
            //             });
            //         }

            //     });

            //     t.test('render string', function (t) {
            //         return u.post("/document/render/string", { html, data }).then(function (response) {
            //             console.log(response.body.renderedHtml);
            //             t.ok(response.body.downloadLink, "Should be able to render html string");
            //         });
            //     });

            //     t.test('remove template', function (t) {
            //         return u.post("/document/remove-template", { bucket, blobId }).then(function (response) {
            //             console.log("Remove template");
            //             t.ok(response.body.done, "Should be able to remove template");
            //         }).finally(resolve);
            //     });
        });

    });
};