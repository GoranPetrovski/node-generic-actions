var user = require('./user');
var faccount = require('../fixtures/account');
var pUtils = require('../../../dst/lib/promise-utils');
var Promise = require('bluebird');

/**
 * Prepares a user account (or several user accounts) for testing.
 * @param {...{User}} u the user to prepare.
 * @returns {Promise}
 */
module.exports = function prepare(u) {
    if (arguments.length == 1) {
        var uinfo = faccount.getAuthObject(u.opt);
        return Promise.resolve();
    } else return pUtils.mapSeries([].slice.call(arguments, 0), function(u) { return prepare(u); });
    // don't be tempted to replace with just prepare as the second argument - that causes trouble
 };