Error.stackTraceLimit = 30;
process.env.NODE_ENV = 'test';
require('source-map-support').install();
var P = require('bluebird');
P.longStackTraces();


var t = require('blue-tape');
var mwApp = require('../../../dst/index.js');
var server = require('./server');

var path = require('path');

function deleteAll() {
    return P.resolve();
}


exports.before = function(t) {
//    db.open();
    return P.join(deleteAll(), server.init(mwApp))
};
exports.after = function(t) {
//    db.close();
    server.get().close();
    t.end();
};


exports.delay = function(time) {
    return new P(function(fulfill) {
        setTimeout(fulfill, time);
    });
};

exports.test = function() {
    t.test('db-open', exports.before);
    t.test.apply(t, arguments);
    t.test('db-close', exports.after);
};

exports.errorTest = function errorTest(t, action, message) {
    return P.try(function() { return action.then(function() { t.notOk(true, message); }); }).catch(function(err) {
        t.ok(err, message);
    });
};