var mwtest = require('middleware-tester'),
    Promise = require('bluebird');

var server;

/**
 * Initializes the middleware test server. This function is asynchronous and will not wait for the server to initialize
 * before returning.
 * @param mw
 */
exports.init = function(mw) {
    mwtest(mw).then(function(server_) {
        server = server_;
    });
};

/**
 * Gets a server instance.
 * @returns a server instance.
 * @throws {Error} if the server has not been initialized yet. You may be missing a call to {@link init()} or it may not
 *                 have completed yet. User {@link getWait()} for a more reliable version.
 */
exports.get = function() {
    if (!server) throw new Error("Server not yet initialized (did you call init()?)");
    return server;
};

/**
 * Delayed version of {@link get()} that will not resolve unless a server instance is available.
 * @param {Number} [timeout] a timeout in milliseconds after which the function gives out and rejects with an error. The
 *                           function will go on forever (or until an instance is available) if no timeout is passed.
 * @returns {Promise} resolves to a server instance.
 */
exports.getWait = function(timeout) {
    var timePassed = 0,
        delay = 100;
    return new Promise(function getServer(resolve, reject) {
        if (server) resolve(server);
        else {
            timePassed += delay;
            if (timeout && (timeout <= timePassed))
                setTimeout(getServer, ((timePassed === 0) && timeout) ? timeout : delay, resolve);
            else reject(new Error("Server not yet initialized"));
        }
    });
};