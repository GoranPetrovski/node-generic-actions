var _ = require('hidash');

var LinkAuthKey = require('../../../dst/routes/lib/link-auth-key');

function Guest(authOpt) {
    this.authOpt = authOpt;
    this.app = require('./server').get().tester();
}

/** @returns {*} a copy of the auth object used by this instance. */
Guest.prototype.getAuthObject = function() { return _.clone(this.authOpt); };

/**
 * Posts JSON data to an URL. Automatically adds the authorizing query string.
 * @param {String} url the URL to post to, relative to the site's root URL.
 * @param {Object} [data] the data to post. You can ignore this if you're piping to the result stream.
 * @param [opt] additional options (I'd be lying if I knew what these are for).
 * @returns {PromiStream}
 */
Guest.prototype.post = function(url, data, opt) {
    var started;
    for (var key in this.authOpt) {
        if (started) url += "&";
        else {
            url += "?";
            started = true;
        }

        url += key + "=" + this.authOpt[key];
    }
    return this.app.postJSON(url, data, opt);
};

/**
 * Gets JSON data from an URL. Automatically adds the authorizing query string.
 * @param {String} url the URL to post to, relative to the site's root URL.
 * @returns {Stream}
 */
Guest.prototype.get = function(url) {
    return this.app.getJSON(url, this.authOpt);
};

/**
 * @param {uuid} guestId the uuid of the guest.
 * @param {string} revision the ID of the revision that this guest is authorized to view.
 * @param {{secretToken:string,user:{secretToken:String},UserAccounts:{id:uuid}}} authGiver
 * the user granting access (the sender of the original message).
 * @param {string} [hashKey] the hash key for this revision and this guest by this user if you have it ready, otherwise
 *                           it'll be generated.
 * @returns {Guest} a new {@link Guest}.
 */
module.exports = function(guestId, revision, authGiver, hashKey) {
    if (!hashKey) hashKey = LinkAuthKey.manager().versionGenerate(revision, guestId, authGiver);
    return new Guest({ revision: revision, from: authGiver.UserAccounts.id, to: guestId, key: hashKey });
};