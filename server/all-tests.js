process.env.NODE_ENV = "test";

var mockServer = require('../mock-server/server');

mockServer.start().then(function() {
    console.log("Started, now requiring integration tests");
    return require('./integration-tests').runTests();
}).then(function() {
    return require('./unit-tests').runTests();
}).then(function() {
    console.log("Stopping mock server");
    return mockServer.stop();
});
