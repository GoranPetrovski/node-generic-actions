#!/usr/bin/env node

var path = require('path');

var migrations = require('anydb-sql-migrations');
var migrationDir = path.resolve(__dirname, '../dst/migrations');

console.log("Working with database", require("../dst/config/index").database.url);
var db = require('../dst/lib/base');
migrations.create(db, migrationDir).run();