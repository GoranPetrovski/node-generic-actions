var fs = require('fs');
var strUtils = require('./dst/lib/string-utils');
var path = require('path');
var Promise = require('bluebird');

var testFiles = fs.readdirSync(path.join(__dirname, 'test', 'integration')).filter(function(path) {
    return strUtils.endsWith(path, '.js');
}).map(function(filename) { return './test/integration/' + filename; });

exports.runTests = function() {
    console.log("runTests");
    return testFiles.reduce(function(p, testFile) {
        console.log("reducing test files", testFile);
        return p.then(function() {
            console.log("Running", testFile);
            return require(testFile).run();
        });
    }, Promise.resolve());
};