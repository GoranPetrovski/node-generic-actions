import _ = require("lodash");
import express = require("express");
import Promise = require("bluebird");
import router = require("simple-router");

import codedError = require("../lib/coded-error");

import bodyParser = require("./lib/body-parser");
import Path = require("path");

import uuid = require("uuid");
import db = require("../lib/base");
import DM = require("../service/document-manager");
import BlobStorage = require("../services/blob-storage");
import S3Action = require("../actions/s3");
var stream = require('stream');
var str = require('string-to-stream');

var app = router();
import config = require('../config/index');

export var route = app.route;

app.post("/string-pdf", bodyParser, req => {
	const DocumentManager = DM.create();
	const html = req.body.html;
	return DocumentManager.stringToPdf(html);
});

app.post("/render/string", bodyParser, req => {
	const DocumentManager = DM.create();
	const html = req.body.html;
	const data = req.body.data;
	console.log(data);
	const renderHtml = DocumentManager.renderHtml(html, data);

	return Promise.resolve({ renderedHtml: renderHtml });
});

app.post("/render/string-pdf", bodyParser, (req, res) => {
	const DocumentManager = DM.create();
	const html = req.body.html;
	const data = req.body.data;
	console.log("Route string-pdf", html, data);
	const renderedHtml = DocumentManager.renderHtml(html, data);
	DocumentManager.stringToPdf(renderedHtml).then(stream => stream.pipe(res));

});

app.post("/save-template", bodyParser, req => {
	console.log("Save template ===");
	const bucket = req.body.bucket;
	const html = req.body.html;
	const path: string = req.body.path;

	var s = new stream.Readable();
	//s.length = new Buffer(html).length;
	s._read = function noop() { }; // redundant? see update below
	s.push(html);
	s.push(null);
	let length = new Buffer(html).length;
	return BlobStorage.create().upload(bucket, path, s, length);
});

app.post("/remove-template", bodyParser, req => {
	const blobId = req.body.blobId;
	const bucket = req.body.bucket;
	return BlobStorage.create().remove(bucket, blobId);
});

app.post("/render-template", bodyParser, (req, res) => {
	const DocumentManager = DM.create();
	const data = req.body.data;
	const link = req.body.link;
	const returnType = req.body["return-type"];

	let S3 = new S3Action.S3();
	let bucket = config.aws.s3.bucket;
	let key = uuid.v4();
	 return DocumentManager.getTemplateFromS3Link(link).then(html => {
		//var rendered = DocumentManager.renderHtml(html, data);
		console.log("DATA", html);
		//return Promise.resolve("AAAA")
		
		// if (returnType == "pdf") {
		// 	 DocumentManager.stringToPdf(<string>html).then(stream => {
		// 		return S3.saveTemplate(bucket, key, stream);
		// 	});
		// } else {
		// 	var s = new stream.Readable();
		// 	s._read = function noop() { };
		// 	s.push(html);
		// 	s.push(null);
		// 	return S3.saveTemplate(bucket, key, s);
		// }


	});
});


// app.post("/render-template", bodyParser, (req, res) => {
// 	const DocumentManager = DM.create();
// 	const data = req.body.data;
// 	const link = req.body.link;
// 	const save = req.body.save;
// 	const returnType = req.body["return-type"];

// 	DocumentManager.getTemplateFromLink(link).then(html => {
// 		var rendered = DocumentManager.renderHtml(html, data);

// 		if (save) {
// 			DocumentManager.save(rendered);
// 		}
// 		if (returnType == "pdf") {
// 			console.log("pdf Stream ...");
// 			DocumentManager.stringToPdf(rendered).then(stream => stream.pipe(res));
// 		}
// 		else {
// 			res.send(rendered);
// 		}
// 	});
// });










