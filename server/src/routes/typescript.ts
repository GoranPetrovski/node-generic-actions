import express = require("express");
import Promise = require("bluebird");
import router = require("simple-router");
import codedError = require("../lib/coded-error");
import bodyParser = require("./lib/body-parser");
import Path = require("path");
import uuid = require("uuid");
import db = require("../lib/base");
import DM = require("../service/document-manager");
import S3Action = require("../actions/s3");
import TSAction = require("../actions/type-script");
import StreamAction = require("../actions/stream-file");
var app = router();
import config = require('../config/index');


export var route = app.route;



app.post("/example", bodyParser, (req, res) => {
	let TS = new TSAction.TypeScript().exampleFn();
    
	return Promise.resolve(true);	
});