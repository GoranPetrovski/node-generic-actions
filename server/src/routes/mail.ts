import express = require("express");
import Promise = require("bluebird");
import router = require("simple-router");
import codedError = require("../lib/coded-error");
import bodyParser = require("./lib/body-parser");
import Path = require("path");
import uuid = require("uuid");
import db = require("../lib/base");
import DM = require("../service/document-manager");
import MailAction = require("../actions/mail");
var app = router();
import config = require('../config/index');


export var route = app.route;

app.post("/build-mail", bodyParser, (req, res) => {
	let Mail = new MailAction.Mail();
	let packageId = req.body.packageId;
	let html = req.body.html;
	let attachmentLink = req.body.attachmentLink;
	let to = req.body.to;
	let subject = req.body.subject;
	return Mail.buildEmailAndPutInS3(packageId,
		html,
		attachmentLink,
		to,
		subject);
});

app.post("/send-mail", bodyParser, (req, res) => {
	let Mail = new MailAction.Mail();
	let packageId = req.body.packageId;
	
	return Mail.sendEmail(packageId);
});