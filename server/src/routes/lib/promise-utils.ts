import Promise = require('bluebird');
/* istanbul ignore next */ 
export class AsyncTaskChain {
    taskChain: Promise<any>;

    constructor() {    
        this.taskChain = Promise.resolve();
    }

    pushTask(asyncTaskFn: (args?: any) => Promise<any>):void {
        this.taskChain = this.taskChain.then(() => (asyncTaskFn)); 
    }

    promise():Promise<any> {
        return this.taskChain;
    }
}