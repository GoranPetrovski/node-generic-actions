import express = require("express");
import Promise = require("bluebird");
import router = require("simple-router");
import codedError = require("../lib/coded-error");
import bodyParser = require("./lib/body-parser");
import Path = require("path");
import uuid = require("uuid");
import db = require("../lib/base");
import DM = require("../service/document-manager");
import S3Action = require("../actions/s3");
import StreamAction = require("../actions/stream-file");
var app = router();
import config = require('../config/index');


export var route = app.route;

app.post("/list", bodyParser, (req, res) => {
	let S3 = new S3Action.S3();
	//"rendered-documents-test-alonso-ger"
	let bucket =  req.body.bucket;
	//id
	let prefix = req.body.prefix;
	return S3.listObject(bucket, prefix);	
});

app.post("/save", bodyParser, (req, res) => {
	let S3 = new S3Action.S3();
	let Stream = new StreamAction.StreamFile();
    const bucket = req.body.bucket;
	const html = req.body.html;
	const path: string = req.body.path;

	return Stream.convertStringToStream(html).then((stream :any)=> {
		return S3.save(bucket, path, stream);	
	});
});

app.post("/get", bodyParser, (req, res) => {
	let S3 = new S3Action.S3();
    const bucket = req.body.bucket;
	const path: string = req.body.path;

	return S3.getDataInS3(bucket, path);	
});

app.post("/=rr", bodyParser, (req, res) => {
	let S3 = new S3Action.S3();
    
	return Promise.resolve(true);	
});