require("source-map-support").install();
import express = require("express");
import http = require("http");
import path = require("path");
import fs = require("fs");
import config = require("./config/index");

let app = express();

console.log("Configuring express...");
app.set("port", process.env.PORT || 8300);
app.enable("trust proxy");

/* istanbul ignore next */
app.use(function (req, res, next) {
    if ("/robots.txt" === req.url) {
        res.type("text/plain");
        res.send("User-agent: *\nDisallow:");
    } else { 
        next();
    }
});

/* istanbul ignore if */ // Can't test this.
if (process.env.NODE_ENV != "test") {
    app.use(require('morgan')("dev"));
}

const exts = /\.js$/,
    routedir = path.join(__dirname, "routes");


console.log("Setting up routes...");

/* istanbul ignore next */
app.get('/', (req, res) => {
    var packagePath = path.join(__dirname, '/../../package.json');
    var readStream = fs.createReadStream(packagePath);
    res.setHeader("ContentType", "application/json");
    readStream.pipe(res);
});

fs.readdirSync(routedir).forEach(function (f) {
    if (f[0] != "." && exts.test(f)) {
        const routePath = path.join(routedir, f);
        const routeModule = require(routePath);
        const usepath = "/" + f.replace(exts, "");
        /* istanbul ignore if */ // this breaks the build process, no need to cover it
        if (!routeModule.route) {
            console.error(`Route module ${f} does not export itself properly`);
            throw new Error(`Unable to set up route ${f}. Check the route module for unexported route variables`);
        }
        app.use(usepath, routeModule.route);
    }
});

export = app;

/* istanbul ignore if */
if (process.env.NODE_ENV !== "test") {
    app.listen(app.get("port"), function () {
        console.log("Express server listening on port " + app.get("port"));
    });
}