import _ = require('lodash')
import Promise = require('bluebird');
//var streamN = require('stream');
import stream = require('stream');
import Stream = stream.Stream;
import codedError = require('../lib/coded-error');
import uuid = require('uuid');

import { DoneResult } from './interfaces';

import ejs = require('ejs');
import https = require('https');

import htmlToPdf = require('phantom-html-to-pdf');
import S3Action = require("../actions/s3");
import BlobStorage = require("../services/blob-storage");
import config = require('../config/index');
var str = require('string-to-stream');
var AWS = require("aws-sdk");


const conversion = htmlToPdf({
    strategy: "dedicated-process"
});

/**
 * Responsible for rendering pdf document
 */

export class DocumentManager {

    stringToPdf(html: string): Promise<Stream> {
        return new Promise((resolve: (s: Stream) => void, reject: (e: any) => void) => {
            conversion({ html: html }, (err: any, response: any) => {
                if (err) reject(err);
                else resolve(response.stream);
            });
        });
    }

    renderHtml(html: any, data: Object): string {
        if (typeof (html) === "string") {
            var rendered = ejs.render(html, data);
            return rendered;
        }
        else {
            return ejs.renderFile(html, data, function (err, result) {
                return ejs.render(html, data);

            });
        }
    }

    getTemplateFromLink(link: string) {
        return new Promise((resolve, reject) => {
            if (_.isEmpty(link)) return reject(new Error("you must set link"));
            if (link.substr(0, 5) != "https") {
                console.log(link.substr(0, 5));
                reject(new Error("your url must be with https protocol"));
            }
            https.get(link, function (res) {
                var buf = "";
                res.on('data', chunk => {
                    buf += chunk;
                });
                res.on('end', () => {
                    return resolve(buf);
                });
            });
        });
    }

    getTemplateFromS3Link(link: string): Promise<string | Stream> {
        return new Promise<string | Stream>((resolve, reject) => {
            if (_.isEmpty(link)) return reject(new Error("you must set link"));
            console.log("LINK", link);
            if (link.indexOf('s3') == -1) {
                return reject(new Error("link is not from S3"));
            }
            let S3 = new S3Action.S3();
            let splitArray = _.split(link, '/');

            let packageId = (splitArray.length > 0) ? splitArray[splitArray.length - 1] : "";
            console.log("Package Id", packageId);
            let bucket = "";
            S3.getDataInS3(bucket, packageId).then(data => {
                console.log("get temp", data);
                var read = AWS.util.buffer.toStream(data);
                var buf = "";
                read.on('data', (chunk: any) => {
                    buf += chunk;
                });
                read.on('end', () => {
                    console.log("Buff", buf);
                    return resolve(buf);
                });

            });
        });
    }

    renderTemplate(link: string, data: Object, save: boolean, returnType: string) {
        return new Promise<{ template: string }>((resolve, reject) => {

            this.getTemplateFromLink(link).then(res => {
                //this.getTemplate("save-key-6-3").then(res => {
                var html = res;
                if (typeof (html) === "string") {
                    var rendered = ejs.render(html, data);
                    resolve({ template: rendered });
                }
            });

        });
    }

    getTemplate(key: string) {
        let S3 = new S3Action.S3();
        return S3.getTemplate(key);
    }

}

export function create() {
    return new DocumentManager();
}