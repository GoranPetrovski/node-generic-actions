export interface NewQuote {
    userId: string
    coverAmmount: number
    inflationFighter: boolean
    moratorumPeriod: number
    premiumType: number
    premiumRate: number
}

export interface DoneResult {
    done: boolean
}