exports.defaultGuiLanguage = 'en';

exports.emailOptions = {}; // Automatically uses the local mail server.

exports.domain = "hoorray.com";

exports.loggerType = 'file';
exports.logFilePath = require('path').resolve(__dirname, '../logs.txt');


exports.baseAddress = "https://hoorray.com";

exports.database = {
    url: 'mysql://dbadmin:GehtB8T!-t@db-timelineservice.hoorray-internal.com:3306/timelineservicedb',
    connections: { min: 1, max: 4 }
};

exports.services = {
    logic: { url: "http://logicservice.hoorray-internal.com" }
};