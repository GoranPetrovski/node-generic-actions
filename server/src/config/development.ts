// add development configuration here (we would need to replace it for hooray)
exports.baseAddress = "http://localhost:8080";

exports.domain = "hooray";

exports.loggerType = 'file';
exports.logFilePath = require('path').resolve(__dirname, '../logs.txt');

exports.defaultGuiLanguage = 'en';

exports.registrationWhitelist = [
    /.*/
];

exports.database = {
    url: "mysql://dbadmin:FHvRg6IXWb@rds-services-devtest.czrbzjn5kgyp.eu-west-1.rds.amazonaws.com:3306/documentrenderingdb",
    connections: { min: 1, max: 4 }
};

exports.AWSSettings = {
    region: "eu-central-1"
}

var baseAddress = "http://ecs-FABER-EcsElast-1TAV4JU1NLV5A-13304223.eu-west-1.elb.amazonaws.com";
//var baseAddress = "http://localhost";

exports.services = {
    blobStorage: { url: baseAddress + ":7900" },
}

// exports.services = {
//     blobStorage: { url: baseAddress + ":8200" },
// }

exports.aws = {
    S3: {
        region: "eu-central-1",
        bucket: "email-packages"
    },
    Dynamodb: {
        table: "AnalyticsStorage",
        region: "eu-central-1",
    }
}