// add development configuration here (we would need to replace it for hooray)
exports.baseAddress = "http://localhost:8080";

exports.domain = "swissre";

exports.loggerType = "file";
exports.logFilePath = require("path").resolve(__dirname, "../logs.txt");

exports.defaultGuiLanguage = "en";

exports.registrationWhitelist = [
    /.*/
];

exports.database = {
    url: "mysql://dbadmin:FHvRg6IXWb@rds-fabertest.czrbzjn5kgyp.eu-west-1.rds.amazonaws.com:3306/documentrenderingdb", // TODO Replace if needed
    connections: { min: 1, max: 4 }
};