// add test configuration here
console.log('exporting test config');

exports.baseAddress = "http://localhost:8500";
exports.domain = "hooray";

exports.mockSession = true;

exports.loggerType = 'file';
exports.logFilePath = require('path').resolve(__dirname, '../logs.txt');

exports.database = {
    url: 'sqlite://',
    connections: { min: 1, max: 4 }
};

exports.AWSSettings = {
    region: "eu-central-1"
}

const baseUrl = "http://localhost:3456";

exports.services = {
    blobStorage: {
        url: baseUrl + "/blob-storage"
    },
}

exports.aws = {
    S3: {
        region: "eu-central-1",
        bucket: "email-packages"
    },
    Dynamodb: {
        table: "AnalyticsStorage",
        region: "eu-central-1",
    }
}