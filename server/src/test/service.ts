import tt = require('./lib/test');
import db = require('../lib/base');
import uuid = require('uuid');
import Promise = require('bluebird');
import _ = require('lodash');
import fs = require('fs');

import DM = require('../service/document-manager');

export function run() {
    return new Promise((resolve: () => void) => {

        tt.test('Document rendering service', t => {
            let DocumentManager = DM.create();

            const html = "<html><body><h1>Hello <%= user.name %> </h1></body></html>";
            const data = {
                user: {
                    name: "Jovan"
                }
            }
            const downloadLink = "https://rendered-documents-test-alonso-ger.s3-eu-west-1.amazonaws.com/03c8f4f2-88cb-496a-a9d5-5b77cb3e3245";
            const save = true;
            const returnType = "pdf";

            t.test("document rendering", t => {
                const renderedString = DocumentManager.renderHtml(html, data);
                t.ok(renderedString, "Should be able to render html");
                t.ok(renderedString.indexOf(data.user.name) != -1, "Placeholder should be replaced");
                //return Promise.resolve();
                return DocumentManager.stringToPdf(html).then(response => {
                    t.ok(response, "should be able to convert string to pdf");
                    //return DocumentManager.urlToPdf('https://www.google.com');
                }).finally(resolve);
        
            });

            t.test('get template by link', t => {
                return DocumentManager.getTemplateFromLink(downloadLink).then(response => {
                    console.log(response);
                    t.ok(response, "should be able to get template");
                }).finally(resolve);
            });

            t.test('render temlate', t => {
                return DocumentManager.renderTemplate(downloadLink, data, save, returnType).then(response => {
                    console.log(response.template);
                    t.ok(response.template, "should be able to render template");
                }).finally(resolve);
            });
        });
    });
}

