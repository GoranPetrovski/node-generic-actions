if (process.env.NODE_ENV != 'test') {
    console.error("Not running in test environment, running in " + process.env.NODE_ENV);
    console.error("Please use the test runner script: scripts/run-test.sh to run the test");
    process.exit();
}

// These lines are pretty much useless now.
process.env.NODE_ENV = 'test';
if (!process.env.NO_BLUEBIRD_DEBUG)
    process.env.BLUEBIRD_DEBUG = 1;
if (!process.env.NO_SOURCEMAPS)
    require('source-map-support').install();

import Promise = require('bluebird');
import t = require('blue-tape');
import SQL = require('anydb-sql');

// import db = require('../../lib/base');

export import Tester = t.Tester;

function deleteAll() {
    return Promise.resolve();
}

function before() {
    // db.open();
    return deleteAll();
}
function after(t:Tester) {
    // db.close();
    t.end();
}

export function test(description:string, test:(t:t.Tester) => void) {
    t.test(' = BEGIN: ' + description, before);
    t.test.apply(t, arguments);
    t.test(' = END: ' + description, after);
}

export function errorTest(t:t.Tester, action:Promise<any>, expectedCode:number, message:string) {
    return Promise.try(() => action.then(() => { t.notOk(true, message); })).catch(err => {
        t.equals(err.code, expectedCode, message);
    });
}