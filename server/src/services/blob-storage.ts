var stream = require("stream");
import Promise = require('bluebird');

import Service = require("../lib/service");
import codedError = require('../lib/coded-error');

import config = require("../config/index");
import { Stream } from "stream";


export class BlobStorage extends Service {

    constructor() {
        super(config.services.blobStorage.url);
    }

    remove(bucket: string, blobId: string): Promise<{ done: boolean }> {
        const removeURL = `/files/${bucket}/remove`;

        return this.post(removeURL, { blobId: blobId }).then(Service.extractBody);
    }

    upload(bucket: string, path: string, stream: Stream, length: number): Promise<any> {
        if (!stream) return Promise.reject(codedError(400, "Stream is missing"));
        if (!stream.pipe) return Promise.reject(codedError(400, "Invalid stream"));
        
        const updateURL = `/files/${bucket}/upload/${path}`;
        console.log("Upload", length);
        return stream.pipe(this.postRaw(updateURL, { 'content-length': length })).promise().then(Service.extractBody);

    }

}

export function create() { return new BlobStorage(); }