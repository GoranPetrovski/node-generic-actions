declare module "phantom-html-to-pdf" {
    import { Stream } from "stream";

    function htmlToPdf(options?: {
        numberOfWorkers?: number;
        timeout?: number;
        tmpDir?: string;
        portLeftBoundary?: number;
        portRightBoundary?: number;
        host?: string;
        strategy?: "phantom-server"|"dedicated-process";
        phantomPath?: string;
        maxLogEntrySize?: string;
        proxy?: any;        // TODO Not sure about the type
        "proxy-type"?: any; // TODO Not sure about the type
        "proxy-auth"?: any; // TODO Not sure about the type
    }): (options: {
        html?: string;
        header?: string;
        footer?: string;
        url?: string;
        printDelay?: number;
        waitForJS?: boolean;
        waitForJSVarName?: string;
        allowLocalFileAccess?: boolean;
        paperSize?: {   // TODO Types of these?
            format?: any;
            orientation?: any;
            margin?: any;
            width?: any;
            height?: any;
            headerHeight?: any;
            footerHeight?: any;
        };
        customHeaders?: any[];  // TODO Type?
        settings?: {
            javascriptEnabled?: boolean;
            resourceTimeout?: number;
        };
        viewportSize?: {
            width: number;
            height: number;
        };
        format?: { quality: number }
    }, callback: (err?: Error, response?: {
        stream: Stream
    }) => void) => void;

    export = htmlToPdf
}