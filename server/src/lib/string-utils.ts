/**
 * Determines whether a string ends with another string.
 * @param {String} str the string.
 * @param {String} suffix the suffix.
 * @returns {boolean} a value indicating whether the string ends with the specified suffix.
 */
function _endsWith(str:string, suffix:string) {
    return str.indexOf(suffix, str.length - suffix.length) != -1;
}

/**
* Determines whether a string ends with one of several other strings.
* @param {String} str the string.
* @param {String[]} suffixes the potential suffixes.
* @returns {boolean} a value indicating whether the string ends with at least one of the specified suffixes.
*/
export function endsWith(str:string, ...suffixes:string[]) {
    for (var i = 0; i < suffixes.length; i++) if (_endsWith(str, suffixes[i])) return true;
    return false;
}

/**
 * Determines whether a string starts with another string.
 * @param str the string.
 * @param prefix the prefix.
 * @returns {boolean} a value indicating whether the string starts with the specified prefix.
 */
/* istanbul ignore next */ 
export function startsWith(str:string, prefix:string) { return str.indexOf(prefix) == 0; }