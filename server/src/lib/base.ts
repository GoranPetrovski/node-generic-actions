import anydbSQL = require('anydb-sql');
import config = require('../config/index');

module db {
    export type Transaction = anydbSQL.Transaction
}

var db = anydbSQL(config.database);
export = db;