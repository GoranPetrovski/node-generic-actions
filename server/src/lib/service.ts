import bl = require('bl');
import _ = require('lodash');
import http = require('http');
import https = require('https');
import qs = require('querystring');
import Promise = require('bluebird');
import through = require('through2');
import duplexer = require('duplexer2');

import strUtils = require('./string-utils');
import codedError = require('./coded-error');

http.globalAgent.maxSockets = 3000;
https.globalAgent.maxSockets = 3000;

interface Dictionary<T> {
    [index: string]: T;
}

/* istanbul ignore next */ // This never happens, but feel free to add it back if it does.
function replaceAll(str: string, context: Dictionary<string>) {
    var reg = /:([^\/]+)/;
    var match: RegExpMatchArray;
    var names: Dictionary<boolean> = {};
    while ((match = str.match(reg))) {
        let name = match[1], inner = match[0];
        str = str.substr(0, match.index) + context[name] + str.substr(match.index + inner.length);
        names[name] = true;
    }
    return str;
}

interface WithBody<T> {
    body: T
    headers: Dictionary<string>
}

namespace Service {
    export type Response<T> = WithBody<T>;
}

interface ThenableDuplexer<T> extends NodeJS.ReadWriteStream {
    then: (succ: (value: WithBody<T>) => any, err?: (error: any) => any) => Promise<any>
    expect: (codes: number[]) => ThenableDuplexer<T>
    promise: () => Promise<WithBody<T>>
}

interface NewRequestOptions {
    url?: string
    method?: string
    body?: Buffer
    json?: boolean
    query?: Dictionary<string>
    headers: Dictionary<any>
}

interface RequestDetails {
    url: string
    query?: Dictionary<string>
    method: string
    headers?: Dictionary<any>
    agent: boolean | { maxSockets?: number }
}

interface RequestOptions {
    url: string
    query: Dictionary<string>
    method: string
    headers: Dictionary<any>
    body: any
    json?: boolean
}

/* istanbul ignore next */ // Will never test communication with other services locally.
class Service {
    private protocol: string;
    private hostname: string;
    private defaultPath: string;
    private port: number;

    constructor(private baseUrl: string) {
        var parts = baseUrl.split("/");
        this.protocol = parts[0].substring(0, parts[0].length - 1);
        var parts2 = parts[2].split(":");
        this.hostname = parts2[0];
        if (parts2[1]) this.port = parseInt(parts2[1]);
        this.defaultPath = parts.slice(3).join("/");
        if (!strUtils.startsWith(this.defaultPath, "/")) this.defaultPath = "/" + this.defaultPath;
        // console.log("Service wrapper initialized", baseUrl);
    }

    protected getDefaultOpt(headers: Dictionary<string> = {}): NewRequestOptions {
        return {
            headers: _.defaults<Dictionary<string>, Dictionary<string>>(headers, {
                'X-Requested-With': "XMLHttpRequest"
            })
        };
    }

    private request<T>(opt: RequestOptions) {
        let expectedCodes = [200, 201];

        let inp = through(), out = through();
        let p = new Promise((resolve, reject) => {
            let path = replaceAll(opt.url, opt.query);
            /* istanbul ignore if */ // Trim a / if needed
            if (strUtils.startsWith(path, "/") && strUtils.endsWith(this.defaultPath, "/"))
                path = this.defaultPath.substring(0, this.defaultPath.length - 1) + path;
            else path = this.defaultPath + path;
            let reqOptions = {
                hostname: this.hostname,
                method: opt.method,
                path: path,
                port: this.port,
                headers: opt.headers || {},
                agent: false
            };

            let postdata: Buffer = undefined;
            if (opt.body != null) {
                postdata = new Buffer(JSON.stringify(opt.body));
                reqOptions.headers["Content-Type"] = "application/json";
                reqOptions.headers["Content-Length"] = postdata.length;
            }

            if (opt.query != null)
                reqOptions.path += "?" + qs.stringify(opt.query);

            function handleRequest(res: any) { // TODO deanify
                res.on("error", reject);
                res.pipe(new bl((err: Error, buf: Buffer) => {
                    /* istanbul ignore if */ // Unexpected error case, not tested.
                    if (err) return reject(err);
                    let unparsed = res.body = buf.toString();
                    if (opt.json)
                        try {
                            /* istanbul ignore if */ // Unexpected error case, not tested.
                            if (res.body.indexOf("Cannot " + reqOptions.method.toUpperCase()) === 0)
                                err = codedError(404, `Missing endpoint: ${reqOptions.method} ${reqOptions.path}`);
                            else res.body = JSON.parse(res.body);
                        } catch (e) {
                            // res.body = e;
                            /* istanbul ignore next */ // Unexpected error case, not tested.
                            err = new Error(res.body);
                        }
                    if (!_.includes(expectedCodes, res.statusCode)) {
                        console.log(`Error response from ${reqOptions.method} ${reqOptions.hostname} ${reqOptions.path}`, res.statusCode, res.body);
                        /* istanbul ignore if */ // Unexpected error case, not tested.
                        if (!err) {
                            let msg = res.body.stack || unparsed;
                            let body = res.body.message || res.body;
                            err = res.statusCode ?
                                codedError(res.statusCode, (body != null ? body.toString() : unparsed) + ` from ${reqOptions.method} ${reqOptions.hostname} ${reqOptions.path}`) :
                                new Error(`HTTP code ${res.statusCode} ${msg}`);
                        }
                    }
                    if (err) reject(err);
                    else resolve(res);
                }));
                res.pipe(out);
            }

            let request: (options: any, callback: Function) => http.ClientRequest;
            switch (this.protocol.toLowerCase()) {
                case "http": request = http.request; break;
                case "https":
                    /* istanbul ignore next */
                    request = https.request;
                    /* istanbul ignore next */
                    break;
                default:
                    /* istanbul ignore next */
                    throw new Error("Unknown protocol: " + this.protocol);
            }
            let req = request(reqOptions, handleRequest);
            req.on("error", reject);

            if (opt.body != null || opt.method == "GET" || opt.method == "DELETE")
                req.end(postdata);
            else
                inp.pipe(req);
        });

        let ret = <ThenableDuplexer<T>>duplexer(inp, out);
        ret.then = (succ, err) => p.then(succ, err);
        /* istanbul ignore next */ // This never happens, but feel free to add it back if it does.
        function expect(codes: number[]) {
            expectedCodes = codes;
            return ret;
        }
        ret.expect = expect;
        ret.promise = () => <Promise<WithBody<T>>>p;
        return ret;
    }

    private _get<T>(url: string, query: Dictionary<string> = {}, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'GET';
        opt.query = query || {};
        return this.request<T>(<RequestOptions>opt);
    }
    private _post<T>(url: string, body?: Buffer, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'POST';
        opt.body = body;
        return this.request<T>(<RequestOptions>opt);
    }

    private _delete<T>(url: string, query: Dictionary<string> = {}, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'DELETE';
        opt.query = query || {};
        return this.request<T>(<RequestOptions>opt);
    }

    private getJSON<T>(url: string, query: Dictionary<string> = {}, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.json = true;
        return this._get<T>(url, query, opt);
    }

    private postJSON<T>(url: string, body: Buffer, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.json = true;
        return this._post<T>(url, body, opt);
    }

    private deleteJSON<T>(url: string, query: Dictionary<string> = {}, opt: NewRequestOptions = this.getDefaultOpt()) {
        opt.json = true;
        return this._delete<T>(url, query, opt);
    }

    post<T>(url: string, what: any, headers: Dictionary<string> = {}) {
        return this.postJSON<T>(url, what, this.getDefaultOpt(headers));
    }

    get<T>(url: string, qs?: Dictionary<any>, headers: Dictionary<string> = {}) {
        return this.getJSON<T>(url, qs, this.getDefaultOpt(headers));
    }

    delete<T>(url: string, qs?: Dictionary<any>, headers: Dictionary<string> = {}) {
        return this.deleteJSON<T>(url, qs, this.getDefaultOpt(headers));
    }

    postRaw(url: string, headers: Dictionary<any> = {}) { return this._post<string>(url, undefined, this.getDefaultOpt(headers)); }

    /* istanbul ignore next */ // TODO Add once you've started testing with this, if ever.
    getRaw(url: string, qs?: Dictionary<string>) { return this._get<string>(url, qs, this.getDefaultOpt()); }

    /* istanbul ignore next */ // TODO Add if used.
    protected getRequestOptions(opt: RequestDetails) {
        var path = replaceAll(opt.url, opt.query);
        /* istanbul ignore if */ // Trim a / if needed
        if (strUtils.startsWith(path, "/") && strUtils.endsWith(this.defaultPath, "/"))
            path = this.defaultPath.substring(0, this.defaultPath.length - 1) + path;
        else path = this.defaultPath + path;
        return {
            hostname: this.hostname,
            method: opt.method,
            path: path,
            port: this.port,
            headers: opt.headers || {}
        };
    }

    static extractBody<T>(response: WithBody<T>): T { return response.body; }
}

export = Service;