/* istanbul ignore next */ 
import Promise = require('bluebird');
import { ModifyingQuery } from 'anydb-sql';

import chunkedQuery = require('./chunked-query');
import Transaction = require('./transaction-like');

import { Queryable, Chunk, Options } from './chunked-query';

module Transactionable {
    export interface CreationOptions { tx: Transaction }
}
/* istanbul ignore next */ 
class Transactionable {
    public tx:Transaction;
    constructor(opt:Transactionable.CreationOptions) {
        this.tx = opt.tx;
    }
    
    protected isIn<T1,T2>(query:Queryable<T1>, chunkedIn:Chunk<T2>, opt?:Options<T1>):Promise<T1[]> {
        return chunkedQuery.isIn(this.tx, query, chunkedIn, opt);
    }
    protected isNotIn<T1,T2>(query:Queryable<T1>, chunkedNotIn:Chunk<T2>, opt?:Options<T1>):Promise<T1[]> {
        return chunkedQuery.isNotIn(this.tx, query, chunkedNotIn, opt);
    }
}

export = Transactionable;