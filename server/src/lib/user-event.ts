import Event = require('./event');

import Transaction = require('./transaction-like');
/* istanbul ignore next */ 
class UserEvent<T> extends Event<T> {
    constructor(public tx:Transaction, public uid: string, public data:T) {
        super(tx, data);
    }

    public new<U>(data:U):UserEvent<U> {
        return new UserEvent(this.tx, this.uid, data);
    }
}

export = UserEvent;