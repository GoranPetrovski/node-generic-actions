import Transactionable = require('./transactionable');
import Transaction = require('./transaction-like');
import UserEvent = require('./user-event');

module UserTransactionable {
    export interface CreationOptions {
        tx:Transaction
        ownerId?:string
    }
}
/* istanbul ignore next */ 
class UserTransactionable extends Transactionable {
    public tx:Transaction;
    public ownerId: string;

    constructor(opt:UserTransactionable.CreationOptions) {
        super(opt);
        /* istanbul ignore if */ // Unexpected error case.
        if (!this.tx) throw new TypeError("No transaction in creation options");
        this.ownerId = opt.ownerId;
    }

    public event<T>(data:T) {
        return new UserEvent(this.tx, this.ownerId, data);
    }

    /* istanbul ignore next */ // Unused for now.
    protected assertUid() { if (!this.ownerId) throw new Error("Method requires ownerId"); }
}

export = UserTransactionable