interface CodedError extends Error {
    name:string;
    message:string;
    code:number;
    innerError:Error;
    stack:string;
    data?:any
}

/* istanbul ignore next */ // Not testing this.
function CodedError(code:number, message:string/*, err?:Error*/):CodedError {
    var self = <CodedError>new Error(message);
    self.code = code;
//    if (err) {
//        self.innerError = err;
//        self.stack += err.stack;
//    }
    return self;
}

module CodedError {
//    export function is(code:number) {
//        return function(err:CodedError) { return err.code == code; };
//    }
}

export = CodedError;