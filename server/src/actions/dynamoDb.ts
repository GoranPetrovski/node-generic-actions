
import config = require('../config/index');
import uuid = require('uuid');
import stream = require('stream');
import Stream = stream.Stream;

import AWS = require("aws-sdk");

AWS.config.setPromisesDependency(require('bluebird'));
AWS.config.region = config.AWSSettings.region; // TODO check this

var s3 = new AWS.S3();


AWS.config.update({ region: "eu-central-1" });
var docClient = new AWS.DynamoDB.DocumentClient();


export class DynamoDb {

    async createItemDynamoDb(packageId: string) {
        var insertParams = {
            TableName: config.aws.Dynamodb.table,
            Item: {
                PackageId: packageId
            }
        };
        try {
            let data = await docClient.put(insertParams).promise();
            console.log("Put item:", JSON.stringify(data, null, 2));
        } catch (err) {
            console.error("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
        }

    }

    async updateItemDynamoDb(packageId: string, messageId: string){
        var updateParams = {
            ExpressionAttributeNames: {
                "#M": "MessageId"
            },
            ExpressionAttributeValues: {
                ":m": messageId
            },
            Key: {
                "PackageId": packageId
            },
            ReturnValues: "ALL_NEW",
            TableName: config.aws.Dynamodb.table,
            UpdateExpression: "SET #M = :m"
        }
        try {
            let data = await docClient.update(updateParams).promise();
            console.log("Updated item:", JSON.stringify(data, null, 2));
        } catch (err) {
            console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        }
    }

    async execAnalyticsStorage() {
        
        AWS.config.update({ region: "eu-central-1" });
        var dynamoDb = new AWS.DynamoDB();

        var insertParams = {
            TableName: config.aws.Dynamodb.table,
            Item: {
                PackageId: "package-id-111"

            }
        };

        var readParams = {
            TableName: config.aws.Dynamodb.table,
            Key: {
                PackageId: "package-id-111"

            }
        };

        var updateParams = {
            ExpressionAttributeNames: {
                "#M": "MessageId"
            },
            ExpressionAttributeValues: {
                ":m": "message-id-234"
            },
            Key: {
                "PackageId": "package-id-111"
            },
            ReturnValues: "ALL_NEW",
            TableName: config.aws.Dynamodb.table,
            UpdateExpression: "SET #M = :m"
        }

        return docClient.put(insertParams, function (err: any, data: any) {
            if (err) {
                console.error("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
            }
            console.log("Put item:", JSON.stringify(data, null, 2));
            return docClient.get(readParams, function (err: any, data: any) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
                }
                console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
                return docClient.update(updateParams, function (err: any, data: any) {
                    if (err) {
                        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                    }
                    return docClient.get(readParams, function (err: any, data: any) {
                        console.log("Get updeted item succeeded:", JSON.stringify(data, null, 2));
                    });

                });
            });
        });
    }

    exampleDynamoFn(){
        console.log("Example dynamo fn");
    }

}


