
var stream = require('stream');
import streamT = require('stream');
import Stream = streamT.Stream;

var fs = require('fs');
import path = require("path");

export class StreamFile {

    convertStringToStream(str: string): Promise<Stream> {
        var s = new stream.Readable();
        s._read = function noop() { };
        s.push(str);
        s.push(null);
        return Promise.resolve(s);
    }

    convertStreamToString(stream: Stream) {
        let str = "";
        let length = 0;
        stream.on('data', (chunk: any) => {
            str += chunk;
            length += chunk.lenght;
        });
        stream.on('end', () => {
            console.log("String", str);
            return (str);
        });
    }

    writeInFile(rstream: Stream) {
        var wstream = fs.createWriteStream('myFile.txt');
        rstream.pipe(wstream);
        return new Promise(function (resolve) {
            rstream.on('end', function (chunk:any) {
                wstream.end();
                const fileSizeInBytes = fs.statSync("myFile.txt")["size"];
                console.log("Size of file", fileSizeInBytes);
                resolve({ fileSize: fileSizeInBytes });
            });
        });
    }

    readFromFile() {
        let pathFile = path.join(__dirname, '..', '..', '..', 'reference-data.json');
        let readData = fs.readFileSync(pathFile);
        console.log("Length in bytes of read data:", readData.byteLength)

    }


}