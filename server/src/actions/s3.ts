import config = require('../config/index');
import uuid = require('uuid');
import Promise = require("bluebird");
import stream = require('stream');
import Stream = stream.Stream;

import AWS = require("aws-sdk");

AWS.config.setPromisesDependency(Promise);
AWS.config.region = config.AWSSettings.region; // TODO check this

var s3 = new AWS.S3();


export class S3 {

    getTemplate(key: string) {
        let params = {
            Bucket: config.aws.s3.bucket,
            Key: key
        };

        function template(data: AWS.S3.GetObjectOutput) {

            return Promise.resolve({ template: data });
        }

        function cbFn(err: any, data: any) {
            if (err) {
                return Promise.reject(new Error("Params is not correct"));
            }
        }

        return s3.getObject(params, cbFn).promise().then(template);
    }

    getDataInS3(bucket: string, key: string) {
        var s3paramsGet = {
            Bucket: bucket,
            Key: key
        }
        function template(data: AWS.S3.GetObjectOutput) {

            return Promise.resolve({ template: data.Body.toString() });
        }

        return s3.getObject(s3paramsGet).promise().then(template);

    }

    save(bucket: string, key: string, template: Stream) {
        let params = {
            Bucket: bucket,
            Key: key,
            Body: template
        };

        function parseLink(data: AWS.S3.ManagedUpload.SendData) {
            return Promise.resolve({ downloadLink: data.Location });
        }

        return s3.upload(params).promise().then(parseLink);
    }

    listObject(bucket: string, path: string) {

        function processData(data: AWS.S3.ListObjectsOutput): Promise<{
            size: number,
            lastModified: Date,
            path: string
        }[]> {
            let transformData = data.Contents.map(item => ({
                size: item.Size,
                lastModified: item.LastModified,
                path: item.Key
            }));
            return Promise.resolve(transformData);
        }

        return s3.listObjects(
            { Bucket: bucket, Prefix: path }
        ).promise().then(processData);

    }
}
