
var stream = require('stream');
import streamT = require('stream');
import Stream = streamT.Stream;

var fs = require('fs');
import path = require("path");

export class TypeScript {

   exampleFn(){
       let array: Array<number> = [1, 4, 7];

       let tuple : [string,number];
       tuple = ['str', 10]; // ОК
       // tuple = ['2', 0, true]; NOT OK

       //Enum
        enum Color { Red = 2, Green = 5, Blue = 7 };
        let c: Color = Color.Red; 
        let colorName: string = Color[2];
        console.log(`colorName ${colorName}`);

        //Assertion (Cast)
        let exampelStr: any = "I am very proud of you ...";
        let strLen1: number = (<string>exampelStr).length;
         let strLen2: number = (exampelStr as string).length;

   }


}