import _ = require("lodash");
Promise = require("bluebird");
import codedError = require('../lib/coded-error');

import config = require('../config/index');
import fs = require("fs");
import path = require('path');


var AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });
var docClient = new AWS.DynamoDB.DocumentClient();

var s3 = new AWS.S3({ signatureVersion: 'v4' });
var mailcomposer = require('mailcomposer');



var ses = new AWS.SES({
    region: 'eu-west-1'
});

import DynamoDbAction = require("./dynamoDb");
let DynamoDb = new DynamoDbAction.DynamoDb();

export class Mail {
    async buildEmailAndPutInS3(packageId: string,
        html: string,
        attachmentLink: string,
        to: string,
        subject: string) {


        if (_.split(attachmentLink, '.')[1].substr(0, 2) == "s3") {
            let splitArray = _.split(attachmentLink, '/');
            let templatePackageId = splitArray[splitArray.length - 1];
            let templateBucket = _.split(attachmentLink, '.')[0].substring(8);
            console.log("Template pac back", templatePackageId, templateBucket);
            let renderTemp = await s3.getObject({ Bucket: templateBucket, Key: templatePackageId }).promise();
            console.log("Render template", renderTemp);


            let messageOption = {
                from: "doxteam6@gmail.com",
                to: to,
                subject: subject,
                html: html,
                attachments: {
                    filename: 'nekoj-nov-file.pdf',
                    content: renderTemp.Body
                }
            };

            let mail = mailcomposer(messageOption);

            return mail.build(function (err: any, message: any) {
                if (err) {
                    console.log("Build error", err);
                }
                var s3paramsPut = {
                    Bucket: config.aws.S3.bucket,
                    Key: packageId,
                    Body: message
                }
                return s3.putObject(s3paramsPut, function (err: any, data: any) {
                    if (err) {
                        console.log(err);
                    }
                    console.log("Data is put in storage success", data.byteLength);
                });
            });
        }
    }

    async sendEmail(packageId: string) {
        var s3paramsGet = {
            Bucket: config.aws.S3.bucket,
            Key: packageId
        }

        let data = await s3.getObject(s3paramsGet).promise();
        let message = await ses.sendRawEmail({ RawMessage: { Data: data.Body } }).promise();

        let messageId = message.MessageId;

        console.log("Message Id", messageId);
        return DynamoDb.updateItemDynamoDb(packageId, messageId);

    }
}