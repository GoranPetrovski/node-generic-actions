var fs = require('fs');
var strUtils = require('./dst/lib/string-utils');
var path = require('path');
var Promise = require('bluebird');

fs.readdirSync(path.join(__dirname, 'dst', 'test')).filter(function(path) {
    return strUtils.endsWith(path, '.js');
}).forEach(function(filename) { require('./dst/test/' + filename); });

var testFiles = fs.readdirSync(path.join(__dirname, 'dst', 'test')).filter(function(path) {
    return strUtils.endsWith(path, '.js');
}).map(function(filename) { return './dst/test/' + filename; });

exports.runTests = function() {
    console.log("runTests");
    return testFiles.reduce(function(p, testFile) {
        console.log("reducing test files", testFile);
        return p.then(function() {
            console.log("Running", testFile);
            return require(testFile).run();
        });
    }, Promise.resolve());
};