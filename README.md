##Endpoints##

### POST /document/string-pdf

Generates pdf file for a given string.

POST parameters:

- **html** - string that contains the content for pdf rendering  (HTML recommended).

Returns a file stream

### POST /document/render/string

Uses EJS to substitue the placeholder (personalize the content).

POST parameters:

- **html** - string that contains the content for ejs rendering  (HTML recommended).
- **data** - object with data to be replaced

Returns

- **renderedHtml** - personalized string


### POST /document/render/string-pdf


POST parameters:

- **html** - string that contains the content for pdf rendering  (HTML recommended).
- **data** - object with data to be replaced.

Returns a file stream

GET parameters:

Returns a list of buckets