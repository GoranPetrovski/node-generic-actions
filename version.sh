#!/bin/sh

# Commits package.json with an incremented version number and tags the commit with the version.
# Parameter usage:
# -v|--version: <newversion> | major | minor | patch | premajor | preminor | prepatch | prerelease | from-git | none
#               (defaults to patch)
# -p|--push: pushes the tags and current commits to the develop repository

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    "--version")    set -- "$@" "-v" ;;
    "--push")       set -- "$@" "-p" ;;
    *)              set -- "$@" "$arg"
  esac
done

PUSH=0
VERSIONTYPE="patch"
OPTIND=1
while getopts "v:p" opt; do
    case "$opt" in
        "v") VERSIONTYPE="${OPTARG}";;
        "p") PUSH=1                 ;;
    esac
done

if [ "${VERSIONTYPE}" != "none" ]; then
    npm version $VERSIONTYPE
else
    git describe --tags
fi

if [ "${PUSH}" = 1 ]; then
    git push origin develop --tags
fi