This folder contains a server app/module that mocks the different services aggregated by the logic service so that,
during testing, the logic microservice does not depend on the state of services that are outside of the scope of the
test.

##How do I use it?##

The mock server is already set up to run automatically when a test starts and shut down when all tests end if you're
using the `dbtest` libraries (both versions, for integration tests and unit tests).

##How do I add new endpoints to a service?##

See the existing files in `routes` for examples. Make sure to document the new service here.

##How do I add new services?##

Each file in `routes` is considered a service. To add a service you need to:

- Create a new module in `routes` that exports the routes to mock. See the existing files for examples on how to do
  this.
- Add an entry in the test configuration (should be `[project root]\server\src\config\test.ts`) under `services`.

So, to add a new service called `myservice`, you would create `routes\myservice.js`, then in `test.ts` you would add
`myservice: { url: baseUrl + '/myservice' }` to the `exports.services` object.