exports.randomInt = function randomInt(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; };

exports.randomBoolean = function randomBoolean() { return !(Math.random()+.5|0); };