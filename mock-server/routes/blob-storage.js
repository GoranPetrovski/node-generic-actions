var _ = require("lodash");
var uuid = require("uuid");
var Promise = require("bluebird");
var router = require('simple-router');

var bodyParser = require('../../server/dst/routes/lib/body-parser');
var codedError = require('../../server/dst/lib/coded-error');
var AWS = require('aws-sdk');
var S3 = new AWS.S3();
var app = router();

var S3Action = require('../../server/dst/actions/s3');
var link = "https//exmple-link";

app.post('/files/:bucket/upload/*path', req => {
    console.log("Mock-upload");
    const path = req.params.path;
    const bucket = req.params.bucket;
    const stream = req;

    let S3 = new S3Action.S3();
    return S3.saveTemplate(bucket, path, stream);
});

app.post('/files/:bucket/remove', bodyParser, (req, res) => {
    console.log("Mock-remove");
    if (!req.body.blobId) res.answer(400, "Missing blobId parameter");
    return Promise.resolve({done: true});
});

exports.route = app.route;