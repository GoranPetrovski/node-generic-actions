var fs = require('fs');
var path = require('path');
var express = require('express');
var Promise = require('bluebird');
var errorHandler = require('../server/dst/routes/lib/error-handler');

var app = express();

var exts = /\.js$/;
var routedir = path.join(__dirname, 'routes');


app.set('port', 3456);
app.enable('trust proxy');

fs.readdirSync(routedir).forEach(function (f) {
    if (f[0] != '.' && exts.test(f)) {
        var routePath = path.join(routedir, f);
        var routeModule = require(routePath);
        var usepath = '/' + f.replace(exts, '');
        if (!routeModule.route) {
            console.error("Route module " + f + " does not export itself properly");
            throw new Error("Unable to set up route " + f + ". Check the route module for unexported route variables");
        }
        console.log("Loaded", usepath);
        app.use(usepath, routeModule.route);
    }
});

var isRunning = false;
var server;
exports.start = function () {
    return new Promise(function (resolve) {
        server = app.listen(app.get('port'), function () {
            isRunning = true;
            resolve();
        });
    });
};

exports.stop = function () {
    return new Promise(function (resolve, reject) {
        if (isRunning)
            try {
                console.log('stopping 1', isRunning);
                server.close(function (err) {
                    console.log('stopping 2');

                    if (err) reject(err);
                    else {
                        isRunning = false;
                        resolve();
                    }
                });
            } catch (e) {
                isRunning = false;
                console.log('stopping 3');
                console.warn("Server already closed");
                resolve();
            }
        else {
            console.warn("Server is not running");
            isRunning = false;
            resolve();
        }
    });
};
